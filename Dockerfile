FROM ruby:2.4

RUN apt-get update -qq \
 && apt-get install -yqq nodejs \
 && gem install bundler  --no-ri --no-rdoc

ENV app /app
WORKDIR $app
ADD . $app

RUN bundle install -j $(nproc) --path vendor

EXPOSE 3000

CMD bundle exec rails s -b 0.0.0.0